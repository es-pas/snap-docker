FROM debian:latest

RUN apt -y update && apt -y install wget

# install snap
RUN wget http://step.esa.int/downloads/8.0/installers/esa-snap_sentinel_unix_8_0.sh && \
    chmod +x esa-snap_sentinel_unix_8_0.sh && \
    ./esa-snap_sentinel_unix_8_0.sh -q

RUN ln -s /usr/local/snap/bin/gpt /usr/bin/gpt

# memory settings
RUN sed -i -e 's/-Xmx1G/-Xmx4G/g' /usr/local/snap/bin/gpt.vmoptions

# install python and java
RUN apt update && \
    apt -y install default-jdk python3 python3-pip git maven && \
    python3 -m pip install --user --upgrade setuptools wheel 

ENV JDK_HOME="/usr/lib/jvm/default-java"
ENV JAVA_HOME=$JDK_HOME
ENV PATH=$PATH:/root/.local/bin

# install snappy
RUN cd /tmp/ && \
    git clone https://github.com/bcdev/jpy.git && \
    cd /tmp/jpy/ && \
    python3 setup.py bdist_wheel && \
    mkdir -p /root/.snap/snap-python/snappy/&& \
    cp /tmp/jpy/dist/* /root/.snap/snap-python/snappy/

RUN /usr/local/snap/bin/snappy-conf /usr/bin/python3 && \
    cd /root/.snap/snap-python/snappy/ && \
    python3 setup.py install && \
    ln -s /root/.snap/snap-python/snappy /usr/lib/python3/dist-packages/snappy
